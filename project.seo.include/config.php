<?php

// путь до админки
define('PROJECT_SEO_ADMIN_URL', '/admin/');
// кеширование включно
define('PROJECT_SEO_IS_CACHE', true);
// время кеширования данных
define('PROJECT_SEO_CACHE_TIME', 300);
// сайт в utf-8
// скрипт сам определяет, если нет, нужно установить вручную
//define('PROJECT_SEO_UTF8', true);

// добавляется перед концом тега <head>
define('PROJECT_SEO_HEAD', '<project_seo head/>');
// добавляется в начале тега <body>
define('PROJECT_SEO_BODY_START', '<project_seo body_start/>');
// добавляется перед концом тега <body>
define('PROJECT_SEO_BODY_END', '<project_seo body_end/>');

// разрешенные заголовки, если скрипт отправляет другие, он останавливается, и не никаких замен
/* не функционирует */
define('PROJECT_SEO_ALLOW_HEADER', 'text/plain|text/html');

/* Пользовательские вызовы */
// Для исключени ajax запросов, нужно переписать функцию на свою логику
Project\Seo\Script\UserEvent::set('filterSeo', function() {
    return true;
});
// проверяются отправляемые заголовки,
// return false; - блокируется обработка текста
Project\Seo\Script\UserEvent::set('filterHeader', function(&$arHeader) {
    return true;
});
// можно менять данные, перед заменой
Project\Seo\Script\UserEvent::set('meta', function(&$arItem) {

});

/* REDIRECT */

// Редирект http <-> https	
//define('PROJECT_SEO_REDIRECT_HTTPS', 'to_https');
define('PROJECT_SEO_REDIRECT_HTTPS', 'to_http');
//define('PROJECT_SEO_REDIRECT_HTTPS', '');
// Игнорировать параметры в URL
//define('PROJECT_SEO_REDIRECT_IGNORE_QUERY', 'Y');
define('PROJECT_SEO_REDIRECT_IGNORE_QUERY', 'N');


// Редирект с www или без www	
//define('PROJECT_SEO_REDIRECT_WWW', 'to_www');
//define('PROJECT_SEO_REDIRECT_WWW', 'to_empty');
define('PROJECT_SEO_REDIRECT_WWW', '');


// Редирект со страниц <b>*/index.php</b> на <b>*/</b>,<br>/about/index.php -> <b>/about/</b>
//define('PROJECT_SEO_REDIRECT_INDEX', 'Y');
define('PROJECT_SEO_REDIRECT_INDEX', 'N');


// Редирект со страниц без слеша на слеш,<br>/catalog -> <b>/catalog/</b>
//define('PROJECT_SEO_REDIRECT_SLASH', 'Y');
define('PROJECT_SEO_REDIRECT_SLASH', 'N');


// Редирект с удалением множественных слешей,<br>//news///index.php -> <b>/news/</b>
//define('PROJECT_SEO_REDIRECT_MULTISLASH', 'Y');
define('PROJECT_SEO_REDIRECT_MULTISLASH', 'N');