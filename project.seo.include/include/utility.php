<?php

namespace Project\Seo\Script;

class Utility {

    static public function toWin1251($arItem) {
        return array_map('trim', array_map(function($v) {
                    return iconv('UTF-8', 'WINDOWS-1251', $v);
                }, $arItem));
    }

    static public function toUtf8($arItem) {
        return array_map('trim', array_map(function($v) {
                    return iconv('WINDOWS-1251', 'UTF-8', $v);
                }, $arItem));
    }

}
