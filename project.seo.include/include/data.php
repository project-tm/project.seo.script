<?

namespace Project\Seo\Script;

class Data {

    static public function get($name) {
        static $arResult = false;
        if (empty($arResult)) {
            $arResult = Cache::get('project.seo', function() {
                        $arResult = array(
                            'redirect' => array(),
                            'meta' => array(),
                            'text' => array(),
                            'replace' => array(),
                        );
                        self::readCsv('redirect', function($arData) use(&$arResult) {
                            Event\Redirect::paser($arResult['redirect'], $arData);
                        });
                        self::readCsv('meta', function($arData) use(&$arResult) {
                            Event\Meta::paser($arResult['meta'], $arData);
                        });
                        self::readCsv('text', function($arData) use(&$arResult) {
                            Event\Text::paser($arResult['text'], $arData);
                        });
                        self::readCsv('replace', function($arData) use(&$arResult) {
                            Event\Text::paser($arResult['replace'], $arData);
                        });
                        return $arResult;
                    });
        }
        return isset($arResult[$name]) ? $arResult[$name] : array();
    }

    static public function readCsv($file, $func) {
        $filename = PROJECT_SEO_SOURSE_DIR . '/data/' . $file . '.csv';
        if (file_exists($filename)) {
            $key = -1;
            if (($handle = fopen($filename, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 100000, ";")) !== FALSE) {
                    $key++;
                    if (empty($key)) {
                        continue;
                    }
                    if (PROJECT_SEO_UTF8) {
                        $data = Utility::toUtf8($data);
                    }
                    $func($data);
                };
                fclose($handle);
            }
        }
    }

}
