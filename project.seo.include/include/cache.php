<?

namespace Project\Seo\Script;

class Cache {

    static public function get($cacheId, $func, $time = PROJECT_SEO_CACHE_TIME) {
        if (!PROJECT_SEO_IS_CACHE) {
            return $func();
        }
        $cacheId = 'project.seo:' . $time . ':' . (int) PROJECT_SEO_UTF8 . ':' . (is_array($cacheId) ? implode(':', $cacheId) : $cacheId);
        $file = PROJECT_SEO_SOURSE_DIR . '/cache/' . sha1($cacheId);
        if (file_exists($file)) {
            if ((time() - $time) < filemtime($file)) {
                return include_once($file);
            }
        }
        $arResult = $func();
        file_put_contents($file, '<?php return ' . var_export($arResult, true) . ';');
        return $arResult;
    }

}
