<?php

namespace Project\Seo\Script\Event\Base;

abstract class Event {

    abstract static public function paser(&$arResult, $arData);

    static protected function getUrl() {
        $param = $_SERVER['REQUEST_URI'];
        $page = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        if ($param !== $page) {
            return array(
                $param,
                $page
            );
        } else {
            return array($page);
        }
    }

    static protected function getItem($arResult) {
        if ($arResult) {
            $search = array();
            foreach (self::getUrl() as $url) {
                if (isset($arResult[$url])) {
                    $search[strlen($url)] = $arResult[$url];
                }
            }
            if ($search) {
                arsort($search);
                $arItem = array_shift($search);
                if ($arItem) {
                    return $arItem;
                }
            }
        }
        return false;
    }

}
