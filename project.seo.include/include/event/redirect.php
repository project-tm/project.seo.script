<?

namespace Project\Seo\Script\Event;

use Project\Seo\Script\Data;

include_once (__DIR__ . '/base/event.php');

class Redirect extends Base\Event {

    public static function toUrl($url, $type = '301 Moved permanently') {
        if ($url != $_SERVER['REQUEST_URI']) {
            header('HTTP/1.1 ' . $type);
            header('Location: ' . $url);
            exit;
//            preExit($_SERVER['REQUEST_URI'], $url, $type);
        }
    }

    static public function paser(&$arResult, $arData) {
        if (count($arData) < 2) {
            return;
        }
        $arResult[$arData[0]] = $arData[1];
    }

    public static function OnPageStart() {
        $host = $_SERVER['SERVER_NAME'];
        $protocol = !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http';
        $port = !empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] != '80' && $_SERVER['SERVER_PORT'] != '443' ?
                (':' . $_SERVER['SERVER_PORT']) : '';
        $currentUri = PROJECT_SEO_REDIRECT_IGNORE_QUERY == 'Y' ? parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) : $_SERVER['REQUEST_URI'];
        $isAbsoluteUrl = false;

        switch (PROJECT_SEO_REDIRECT_HTTPS) {
            case 'to_https':
                if ($protocol === 'http') {
                    $protocol = 'https';
                    $url = $currentUri;
                }
                break;
            case 'to_http':
                if ($protocol === 'https') {
                    $protocol = 'http';
                    $url = $currentUri;
                }
                break;
            default:
                break;
        }

        switch (PROJECT_SEO_REDIRECT_WWW) {
            case 'to_www':
                if (substr($_SERVER['SERVER_NAME'], 0, 4) !== 'www.') {
                    $host = 'www.' . $_SERVER['SERVER_NAME'];
                    $url = $currentUri;
                }
                break;
            case 'to_empty':
                if (substr($_SERVER['SERVER_NAME'], 0, 4) === 'www.') {
                    $host = substr($_SERVER['SERVER_NAME'], 4);
                    $url = $currentUri;
                }
                break;
            default:
                break;
        }

        if (PROJECT_SEO_REDIRECT_INDEX == 'Y' || PROJECT_SEO_REDIRECT_SLASH == 'Y' || PROJECT_SEO_REDIRECT_MULTISLASH == 'Y') {
            $changed = false;
            $u = parse_url($currentUri);
            if (PROJECT_SEO_REDIRECT_INDEX == 'Y') {
                $tmp = rtrim($u['path'], '/');
                if (basename($tmp) == 'index.php') {
                    $dname = dirname($tmp);
                    $u['path'] = ($dname != DIRECTORY_SEPARATOR ? $dname : '') . '/';
                    $changed = true;
                }
            }
            if (PROJECT_SEO_REDIRECT_SLASH == 'Y') {
                $tmp = basename(rtrim($u['path'], '/'));
                // add slash to url
                if (substr($u['path'], -1, 1) != '/' && substr($tmp, -4) != '.php' && substr($tmp, -4) != '.htm' && substr($tmp, -5) != '.html') {
                    $u['path'] .= '/';
                    $changed = true;
                }
            }
            if (PROJECT_SEO_REDIRECT_MULTISLASH == 'Y') {
                if (strpos($u['path'], '//') !== false) {
                    $u['path'] = preg_replace('{/+}s', '/', $u['path']);
                    $changed = true;
                }
            }
            if ($changed) {
                $url = $u['path'];
                if (!empty($u['query'])) {
                    $url .= '?' . $u['query'];
                }
            }
        }

        if (isset($url)) {
            if ($isAbsoluteUrl) {
                self::toUrl($url);
            } else {
                self::toUrl($protocol . '://' . $host . $port . $url);
            }
        }

        $arResult = Data::get('redirect');
        if ($url = self::getItem($arResult)) {
            self::toUrl($url);
        }
    }

}
