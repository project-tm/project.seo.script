<?

namespace Project\Seo\Script\Event;

use Project\Seo\Script\Data,
    Project\Seo\Script\UserEvent;

include_once (__DIR__ . '/base/event.php');

class Meta extends Base\Event {

    static public function paser(&$arResult, $arData) {
        if (count($arData) < 1) {
            return;
        }
        $arData = array(
            'URL' => $arData[0],
            'H1' => $arData[1],
            'TITLE' => $arData[2],
            'KEYWORDS' => $arData[3],
            'DESCRIPTION' => $arData[4],
            'META' => $arData[5],
        );
        if (empty($arData['URL'])) {
            return;
        }
        $arResult[$arData['URL']] = $arData;
    }

    public static function OnEndBufferContent(&$content) {
        $arResult = Data::get('meta');
        $pattern = PROJECT_SEO_UTF8 ? 'u' : '';
        if ($arItem = self::getItem($arResult)) {
            foreach (UserEvent::get('meta') as $func) {
                $func($arItem);
            }
            if (empty($arItem['H1'])) {
                preg_match('~<h1[^>]*>(.*)</h1>~imsU' . $pattern, $content, $tmp);
                if (!empty($tmp[1])) {
                    $arItem['H1'] = $tmp[1];
                }
            }
            foreach ($arItem as $key => $value) {
                if ($key != 'H1') {
                    $arItem[$key] = str_replace('#H1#', $arItem['H1'], $value);
                }
                if ($value == '-') {
                    $arItem[$key] = '';
                }
            }

            $res = preg_replace('~(\s)~', '', $arItem['TITLE']);
            if (!empty($res)) {
                $content = preg_replace('~(<title[^>]*>)(.*)(</title>)~imsU' . $pattern, '${1}' . $arItem['TITLE'] . '${3}', $content);
                $content = preg_replace('~(<meta[^>]*property="og:title"[^>]*content=")([^"]*)("[^>]*>)~imsU' . $pattern, '${1}' . addslashes($arItem['TITLE']) . '${3}', $content);
            }
            $res = preg_replace('~(\s)~', '', $arItem['H1']);
            if (!empty($res)) {
                $content = preg_replace('~(<h1[^>]*>)(.*)(</h1>)~imsU' . $pattern, '${1}' . $arItem['H1'] . '${3}', $content);
            }
            $res = preg_replace('~(\s)~', '', $arItem['KEYWORDS']);
            if (!empty($res)) {
                $content = preg_replace('~(<meta[^>]*name="keywords"[^>]*content=")([^"]*)("[^>]*>)~imsU' . $pattern, '${1}' . addslashes($arItem['KEYWORDS']) . '${3}', $content);
            }
            $res = preg_replace('~(\s)~', '', $arItem['DESCRIPTION']);
            if (!empty($res)) {
                $content = preg_replace('~(<meta[^>]*name="description"[^>]*content=")([^"]*)("[^>]*>)~imsU' . $pattern, '${1}' . addslashes($arItem['DESCRIPTION']) . '${3}', $content);
                $content = preg_replace('~(<meta[^>]*property="og:description"[^>]*content=")([^"]*)("[^>]*>)~imsU' . $pattern, '${1}' . addslashes($arItem['DESCRIPTION']) . '${3}', $content);
            }
            $content = str_replace('</head>', $arItem['META'] . '</head>', $content);
        }

        if (PROJECT_SEO_HEAD) {
            $content = str_replace('</head>', PROJECT_SEO_HEAD . '</head>', $content);
        }
        if (PROJECT_SEO_BODY_START) {
            $content = preg_replace('~(<body[^>]*>)~imsU' . $pattern, '${1}' . PROJECT_SEO_BODY_START, $content);
        }
        if (PROJECT_SEO_BODY_END) {
            $content = str_replace('</body>', PROJECT_SEO_BODY_END . '</body>', $content);
        }
    }

}
