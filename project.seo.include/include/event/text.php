<?

namespace Project\Seo\Script\Event;

use Project\Seo\Script\Data;

include_once (__DIR__ . '/base/event.php');

class Text extends Base\Event {

    static public function paser(&$arResult, $arData) {
        if (count($arData) < 3) {
            return;
        }
        $arData = array(
            'CODE' => $arData[1],
            'URL' => $arData[2],
            'TEXT' => $arData[3],
        );
        if (empty($arData['CODE']) or empty($arData['URL']) or empty($arData['TEXT'])) {
            return;
        }
        $arResult[$arData['URL']][self::getCode($arData['CODE'])] = $arData['TEXT'];
    }

    static private function getCode($code) {
        return '<!-- #' . $code . '# -->';
    }

    static public function OnEndBufferContent(&$content) {
        $arResult = Data::get('text');
        if ($arItem = self::getItem($arResult)) {
            $content = str_replace(array_keys($arItem), $arItem, $content);
        }
    }

}
