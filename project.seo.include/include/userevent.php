<?php

namespace Project\Seo\Script;

class UserEvent {

    static private $arEvent = array();

    static public function set($name, $func) {
        self::$arEvent[$name][] = $func;
    }

    static public function get($name) {
        return isset(self::$arEvent[$name]) ? self::$arEvent[$name] : array();
    }

}
