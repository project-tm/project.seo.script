<?php

include_once (__DIR__ . '/include/userevent.php');
include_once (__DIR__ . '/config.php');
include_once (__DIR__ . '/include/debug.php');

/* Для исключени ajax запросов, нужно переписать функцию на свою логику */
foreach (Project\Seo\Script\UserEvent::get('filterSeo') as $func) {
    if ($func() === false) {
        return;
    }
}

define('PROJECT_SEO_SOURSE_DIR', __DIR__);
if (isset($_SERVER['REQUEST_URI'])) {
    define('PROJECT_SEO_IS_ADMIN', stripos($_SERVER['REQUEST_URI'], PROJECT_SEO_ADMIN_URL) === 0);
    if (PROJECT_SEO_IS_ADMIN) {
        return;
    }
} else {
    return;
}

include_once (__DIR__ . '/include/cache.php');
include_once (__DIR__ . '/include/utility.php');
include_once (__DIR__ . '/include/data.php');
include_once (__DIR__ . '/include/event/meta.php');
include_once (__DIR__ . '/include/event/redirect.php');
include_once (__DIR__ . '/include/event/text.php');
include_once (__DIR__ . '/include/event/replace.php');

ob_start();
register_shutdown_function(function() {
    $content = ob_get_clean();
    if (!defined('PROJECT_SEO_UTF8')) {
        $isUft8 = true;
        if (stripos('charset=windows-1251', $content) or stripos('charset=cp1251', $content)) {
            $isUft8 = false;
        }
        define('PROJECT_SEO_UTF8', $isUft8);
    }

    /* проверяются отправляемые заголовки, можно остановить обработку, если скрипт отдает неприемлиемые заголовки */
    $arHeader = headers_list();
    $allow = explode('|', PROJECT_SEO_ALLOW_HEADER);
    foreach (Project\Seo\Script\UserEvent::get('filterSeo') as $func) {
        if ($func($arHeader) === false) {
            return;
        }
    }
//    preExit($arHeader, $allow);

    Project\Seo\Script\Event\Redirect::OnPageStart();
    Project\Seo\Script\Event\Meta::OnEndBufferContent($content);
    Project\Seo\Script\Event\Text::OnEndBufferContent($content);
    Project\Seo\Script\Event\Replace::OnEndBufferContent($content);
    echo $content;
});
