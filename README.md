# Сeo модуль #

Настройки индентичные [сео модуля для битрикс](https://bitbucket.org/project-tm/project.seo)

- [Настройки](#Настройки)
- [Пользовательские вызовы](#Пользовательские-вызовы)
- [Редиректы](#Редиректы)
- [Мета теги](#Мета-теги)
- [Тексты](#Тексты)
- [Замена текстовых блоков](#Замена-текстовых-блоков)

## Ajax, Post запросы
- каждый сайт нужно настроить, чтобы при ajax запросов, отправке форм, сайт нормально функционировал
- всю эту логику нужно заложить в эту функцию
```php
Project\Seo\Script\UserEvent::set('filterSeo', function() {
    return true;
});
```

## Настройки
- путь до админки
```php 
define('PROJECT_SEO_ADMIN_URL', '/admin/');
```
- кеширование включно
```php
define('PROJECT_SEO_IS_CACHE', true);
```
- время кеширования данных
```php
define('PROJECT_SEO_CACHE_TIME', 300);
```
- сайт в utf-8, скрипт сам определяет, если нет, нужно установить вручную
```php
define('PROJECT_SEO_UTF8', true);
```
- добавляет код, перед концом тега <head>
```php
define('PROJECT_SEO_HEAD', '<project_seo head/>');
```
- добавляет код, в начале тега <body>
```php
define('PROJECT_SEO_BODY_START', '<project_seo body_start/>');
```
- добавляет код, перед концом тега <body>
```php
define('PROJECT_SEO_BODY_END', '<project_seo body_end/>');
```


## Пользовательские вызовы
- Для исключени ajax запросов, нужно переписать функцию, на свою логику
```php
Project\Seo\Script\UserEvent::set('filterSeo', function() {
    return true;
});
```
- можно менять данные, перед заменой
```php
Project\Seo\Script\UserEvent::set('meta', function(&$arItem) {

});
```
- проверяются отправляемые заголовки, можно остановить обработку, если скрипт отдает неприемлиемые заголовки
- return false; - блокируется обработка текста
```php
// return false; - блокируется обработка текста
Project\Seo\Script\UserEvent::set('filterHeader', function(&$arHeader) {
    return true;
});
```


## Редиректы
- Редирект http <-> https
```php
define('PROJECT_SEO_REDIRECT_HTTPS', 'to_https');
define('PROJECT_SEO_REDIRECT_HTTPS', 'to_http');
define('PROJECT_SEO_REDIRECT_HTTPS', '');
```

- Игнорировать параметры в URL
```php
define('PROJECT_SEO_REDIRECT_IGNORE_QUERY', 'Y');
define('PROJECT_SEO_REDIRECT_IGNORE_QUERY', 'N');
```

- Редирект с www или без www
```php
define('PROJECT_SEO_REDIRECT_WWW', 'to_www');
define('PROJECT_SEO_REDIRECT_WWW', 'to_empty');
define('PROJECT_SEO_REDIRECT_WWW', '');
```

- Редирект со страниц <b>*/index.php</b> на <b>*/</b>,<br>/about/index.php -> <b>/about/</b>
```php
define('PROJECT_SEO_REDIRECT_INDEX', 'Y');
define('PROJECT_SEO_REDIRECT_INDEX', 'N');
```

- Редирект со страниц без слеша на слеш,<br>/catalog -> <b>/catalog/</b>
```php
define('PROJECT_SEO_REDIRECT_SLASH', 'Y');
define('PROJECT_SEO_REDIRECT_SLASH', 'N');
```

- Редирект с удалением множественных слешей,<br>//news///index.php -> <b>/news/</b>
```php
define('PROJECT_SEO_REDIRECT_MULTISLASH', 'Y');
```php
define('PROJECT_SEO_REDIRECT_MULTISLASH', 'N');
```


## Мета теги
- заменяются `h1`, `title`, `keywords`, `description`, код в конце блок <head>
- заменяется `<meta property="og:title" content="">` и `<meta property="og:description" content="">`
- во всех параметрах (кроме `h1`), можно сделать замены `#H1#`
- если `h1` не заполнен, он берется с кода страницы
- если в тегих указать `«-»`, тег будет заменен на пустой

## Тексты
- для замены текста, необходимо на странице добавить код ```<!-- #CODE# -->```
- для каждого текстового блока можно создать свой код, заполняется в разделе

## Замена текстовых блоков
- заменяют один текстовый блок, на другой